# -*- coding: utf-8 -*-
import os

from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from flask import Flask
from flask import request


app = Flask(__name__)

GP_URL = 'https://'
MERCHANT_NUMBER = os.environ.get('MERCHANT_NUMBER', None)
PAYMENT_GATEWAY_PUBLIC_KEY = os.environ.get('GP_GATEWAY_PUBLIC_KEY', None)
MERCHANT_PRIVATE_KEY_FILE = os.environ.get('GP_MERCHANT_PRIVATE_KEY', None)
MERCHANT_PRIVATE_KEY_PASSWORD = os.environ.get('GP_MERCHANT_PRIVATE_KEY', None)

CERTIFICATE_DIR = os.path.join(
    os.path.dirname(__file__),
    '/certificates/',
)


def password(*args, **kwargs):
    return "Spalena"


def sign_message(message):
    """ Emulate php"""

    if MERCHANT_PRIVATE_KEY_FILE is None:
        raise Exception("Private key filename must be provided.")

    private_key_file_path = os.path.join(CERTIFICATE_DIR, MERCHANT_PRIVATE_KEY_FILE)
    certificate_file = open(private_key_file_path, 'r')
    key = RSA.importKey(certificate_file.read(), MERCHANT_PRIVATE_KEY_PASSWORD)

    h = SHA.new(message)
    signer = PKCS1_v1_5.new(key)
    signature = signer.sign(h)

    return signature


def verify_message(message, signature):
    """
    Validate signature of message

    :param message: Unsigned message sent by payment gateway
    :type data: str
    :param signature: Signed message sent by payment gateway
    :type signature: str
    :return: True if signed message was generated with private key of GP payment gateway, False otherwise
    """
    public_key_file_path = os.path.join(CERTIFICATE_DIR, PAYMENT_GATEWAY_PUBLIC_KEY)
    certificate_file = open(public_key_file_path, 'r')

    key = RSA.importKey(certificate_file.read())
    h = SHA.new(message)

    verifier = PKCS1_v1_5.new(key)

    return verifier.verify(h, signature)


def validate_payment(gateway_response):
    """
    Method validate if sent gateway_response from payment gateway is valid
    """
    ok = True

    requests = {
        'RESULTTEXT': 'OK',
        'PRCODE': '0',
        'SRCODE': '0',
    }

    for key, desired_value in requests.items():
        if not (key in gateway_response.GET and gateway_response.GET[key] == desired_value):
            ok = False

    if not ok:
        # Basic requirements have not met our criteria
        return ok

    data_list = []
    for key in ['OPERATION', 'ORDERNUMBER', 'MERORDERNUM', 'PRCODE', 'SRCODE', 'RESULTTEXT', ]:
        data_list.append(str(gateway_response.GET.get(key, '')))

    digest = str(gateway_response.GET.get('DIGEST', None))
    digest1 = str(gateway_response.GET.get('DIGEST1', None))

    if digest is None or digest1 is None:
        # Digests are key elements for next work
        return False

    data = '|'.join(data_list)
    if not verify_message(data, digest):
        return False

    data_list_with_merchant_number = data_list.append(MERCHANT_NUMBER)

    data_with_merchant_number = '|'.join(data_list_with_merchant_number)

    # This step is optional and ensure maximal authenticity of sent data
    if not verify_message(data_with_merchant_number, digest1):
        return False

    return ok


def generate_payment_link(order_number):
    """
    Generate payment link request
    """
    pass


def validate_close(request):
    """
    Validate close order response
    """
    pass


@app.route('/request/create-order/<int:order_number>')
def request_create_order_link(order_number):
    """
    Generate link for payment request
    """
    return '<a href="%s">Pay</a>' % generate_payment_link(order_number=order_number)


@app.route('/request/close-order/<int:order_number>')
def request_close_order_link(order_number):
    """
    Generate link for close order on GP Gateway

    :param order_number: Order number for link
    :type order_number: int
    """
    return '<a href="%s">Pay order no. %d</a>' % (generate_payment_link(order_number=order_number), order_number)


@app.route('/response/create-order/')
def response_create_order():
    if validate_payment(request):
        return '<span style="color: green;">OK</span>'
    else:
        return '<span style="color: red;">Fail</span>'


@app.route('/response/close-order/')
def response_close_order():
    if validate_close(request):
        return '<span style="color: green;">OK</span>'
    else:
        return '<span style="color: red;">Fail</span>'


if __name__ == '__main__':
    app.run()
